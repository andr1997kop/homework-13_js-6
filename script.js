/*
1.Екранування потрібне для того, щоб можна було виводити в консоль або на
сторінку ті символи, які зарезервовані під використання у коді. Наприклад, 
якщо треба взяти щось в лапки, то використовується екранування, бо браузер інакше
зчитає ці лапки як початок і кінець рядку, як типу даних, наприклад.
2. Можна використати ключове слово 'function' або записати функцію, як значення
якоїсь змінної. Іншими словами оголосити її як вираз.
3. Хоістінг - це коли змінні та функції можна використати
до моменту їх оголошення.
 */
function createNewUser () {
    let newUser = {
        firstName: prompt(`Enter your first name`, ``),
        lastName: prompt(`Enter your last name`, ``),
        birthday: prompt(`Enter date of your birth in dd.mm.yyyy`, ``),
        getLogin() {
            return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`;
        },
        getAge() {
            let now = new Date ();
            let dateOfBirth = new Date(this.birthday.substring(6) + `.`+ this.birthday.substring(3,5) + `.` + this.birthday.substring(0,2));
            let age = new Date(now - dateOfBirth).getFullYear()-1970;
            return age;
        },
        getPassword() {
            return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${this.birthday.substring(6)}`;
        },
    }
    return newUser;
};

let user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());

